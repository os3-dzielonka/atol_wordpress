��    ~        �   �      �
     �
  ,   �
     �
     �
     �
     �
               +     1  �   =     �     �     �     �                         /  
   A     L  
   S     ^  	   n     x     �     �     �     �     �     �     �     �     �                 #   (     L     ]     e     k     w     |     �     �     �     �     �     �  
   �     �  	   �     �     �     �                    -     4     <  	   L     V     g     w     z     �     �     �     �     �     �     �     �     �     �     �                    /     @     F     U  
   d     o     }     �     �     �     �     �     �     �     �  	   �  	                       !     (     0     8     E     L     Q     ]     m  	   v  	   �     �     �     �     �     �     �  3   �       ?        V     j     o  �  w       H        W  
   k  
   v     �     �     �     �     �  �   �  
   �     �     �     �     �     �     �     �     �          #     *     A     U     g     �     �     �  
   �     �     �     �     �               #     0  -   9     g  
        �     �     �     �     �     �     �     �  
   �  
   �            	   )     3     9     >     C     U     \     v     ~     �  
   �     �     �     �     �     �               ,     ?     Z     ^     f  
   m     x          �     �     �     �     �     �  
   �  
   �     �          $  
   *     5     I     ^     y     �     �     �     �     �     �     �     �     �               -     4     :     G     W     l  	   �     �     �  .   �     �     �     �  K   �     K  >   S     �     �     �                U   	   @           '   A   i                                   b       G   .      O       3   V             B   W      {   a         $      7          *                  P               d   T   <   f           (   +   j       Z   e   R   w       ?   m   y       
   [   #   -   N   2       ;   4           h   X               }           r   `   C              :          o       >      F   n   l   5                    g      ,   x   =   S   ]   |   u          ^   Y   M   c       "      !   6   &   p   8   /   k   s   z      v   )   1       \       D                  _   I       ~       t      K   %   Q   L   J   E   9          H   q   0        on  to generate content for Cookie Policy page. Accept Button Action Add New Add New Cookie Type Advanced Always Enabled Arial Arial Black As per latest GDPR policies it is required to take an explicit consent for the cookies. Use this option with discretion especially if you serve EU Background colour Banner Border Colour Bottom Left Bottom Right Button Click Consent Accepted. Consent rejected. Contact Us Cookie Cookie Bar Cookie Duration Cookie ID Cookie Law Settings Cookie List Cookie Policy Cookie Sensitivity Cookie Type Default state Default theme font Delete Delete Cookies Description Disabled Documentation Duration ERROR MIGRATING SETTINGS (ERROR: 2) Edit Cookie Type Enabled Error Extra Large Font Footer From Left Margin GDPR Cookie Consent General Georgia, serif Header Heading Help Links Help and Support Helvetica Large Left Link Live preview Lucida Manage your consent. Medium Message Message Heading Necessary Necessary Cookie New Cookie Type No No consent given. No records found Non-necessary Non-necessary Cookie Nothing found Nothing found in Trash Off On On hide Other Page Policy generator Popup Position Premium Upgrade Privacy Overview Right Sample content Sample heading Sans Serif Save Settings Search Cookies Serif Settings Settings Button Settings Updated. Settings updated. Show Again Tab Show Border? Show More Text Show less Show more Size Small Specify Sticky Success Support Tab Position Tahoma Text Text colour Times New Roman Top Left Top Right Trebuchet Type URL Unable to handle your request. Update Settings Verdana View Cookie Type We would love to help you on any queries or issues. Widget You do not have sufficient permission to perform this operation Your current state: here seconds Project-Id-Version: 
PO-Revision-Date: 2019-11-29 02:55+0100
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ..
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
  w dniu  do generowania zawartości na stronie zasady dotyczące plików cookie. Przycisk AKCEPTUJĘ Działanie Dodaj nowe Dodaj nowy typ plików cookie Zaawansowany Zawsze włączone Arial Arial Black Zgodnie z najnowszymi zasadami Rodo, wymagane jest jednoznaczne wyrażenie zgody na pliki cookie. Użyj tej opcji z rozwagą, zwłaszcza jeżeli znajdujesz się na terenie Unii Europejskiej Kolor tła Baner Kolor ramki W lewym dolnym rogu W prawym dolnym rogu Przycisk Kliknięcie Zgoda przyjęta. Zgoda odrzucona. Skontaktuj się z nami Cookie Pasek Polityka Cookies Czas trwania cookie ID plików cookie Ustawienia plików cookie Lista plików cookie Polityka Cookie Czułość na ciasteczka Typ Cookie Stan domyśłny Domyślna czcionka motywu Usuń Usuwanie plików cookie Opis Wyłączone Dokumentacja Duration BŁĄD PODCZAS MIGRACJI USTAWIEŃ (BŁĄD: 2) Edytuj typ pliku cookie Włączone Błąd Bardzo duży Czcionka Stopka Z lewego marginesu GDPR Cookie Consent Ogólne Georgia, serif Nagłówek Nagłówek Pomocne Linki Uzyskaj pomoc i wsparcie Helvetica Duży Lewo Link Podgląd na żywo Lucida Zarządzaj swoją zgodą. Średni Wiadomość Nagłówek komunikatu Niezbędne Niezbędne cookie Nowy typ plików cookie Nie Nie udzielono zgody. Nie znaleziono pozycji Nieobowiązkowe Nieobowiązkowe cookie Nic nie znaleziono Nie znaleziono nic w koszu Off Włącz Ukryć Pozostałe Strona Generator polityki Popup Pozycja Ulepszenie Premium Prywatność - ogólne Prawo Przykładowa treść Nagłówek Sans Serif Zapisz ustawienia Szukaj Plików Cookies Serif Ustawienia Przycisk USTAWIENIA Ustawienia zapisano. Ustawienia zaktualizowane. Zakładka "Pokaż jeszcze raz" Wyświetlać obramowanie? Tekst pokaż więcej Pokaż mniej Pokaż więcej Rozmiar Mały Określ Lepki Success Wsparcie techniczne Pozycja zakładki Tahoma Tekst Kolor tekstu Times New Roman W lewym górnym rogu W prawym górnym rogu Trebuchet Rodzaj URL Twoja prośba nie mogła zostać zrealizowana. Aktualizuj ustawienia Verdana Wyświetl typ pliku cookie Chcielibyśmy pomóc Tobie, w przypadku wszelkich pytań bądź problemów. Widżet Nie masz wystarczających uprawnień do wykonania tej operacji Twój obecny stan: tutaj sekund 