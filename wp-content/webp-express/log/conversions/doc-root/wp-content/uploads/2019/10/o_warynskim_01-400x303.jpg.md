WebP Express 0.17.2. Conversion triggered with the conversion script (wod/webp-realizer.php), 2019-12-13 21:16:16

*WebP Convert 2.3.0*  ignited.
- PHP version: 7.3.1
- Server software: Apache

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg
- destination: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "none"
- near-lossless: 60
- quality: "auto"
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg
- destination: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg.webp
- default-quality: 70
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- max-quality: 80
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: "auto"
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: version: *0.5.2*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 3 binaries: 
- [doc-root]usr[doc-root]sbin[doc-root]cwebp
- [doc-root]bin[doc-root]cwebp
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Linux)... We do. We in fact have 3
Found 3 binaries: 
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64
Detecting versions of the cwebp binaries found
- Executing: cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]sbin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]sbin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]bin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]bin[doc-root]cwebp: (version: 0.5.2)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 0.5.2
Quality of source is 82. This is higher than max-quality, so using max-quality instead (80)
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice cwebp -metadata none -q 80 -alpha_q '85' -m 6 -low_memory '[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg' -o '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg.webp.lossy.webp' 2>&1

*Output:* 
Saving file '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg.webp.lossy.webp'
File:      [doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg
Dimension: 400 x 303
Output:    22576 bytes Y-U-V-All-PSNR 39.43 41.74 43.58   40.25 dB
block count:  intra4: 414
              intra16: 61  (-> 12.84%)
              skipped block: 0 (0.00%)
bytes used:  header:            218  (1.0%)
             mode-partition:   1956  (8.7%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |   16191 |     160 |     144 |      41 |   16536  (73.2%)
 intra16-coeffs:  |     277 |      93 |      40 |      98 |     508  (2.3%)
  chroma coeffs:  |    3080 |      54 |      90 |     106 |    3330  (14.8%)
    macroblocks:  |      81%|       4%|       4%|       9%|     475
      quantizer:  |      22 |      16 |      11 |      11 |
   filter level:  |      10 |       4 |       2 |       5 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |   19548 |     307 |     274 |     245 |   20374  (90.2%)

Success
Reduction: 37% (went from 35 kb to 22 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: version: *0.5.2*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 3 binaries: 
- [doc-root]usr[doc-root]sbin[doc-root]cwebp
- [doc-root]bin[doc-root]cwebp
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Linux)... We do. We in fact have 3
Found 3 binaries: 
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64
Detecting versions of the cwebp binaries found
- Executing: cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]sbin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]sbin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]bin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]bin[doc-root]cwebp: (version: 0.5.2)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 0.5.2
Trying to convert by executing the following command:
nice cwebp -metadata none -q 80 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg' -o '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg.webp.lossless.webp' 2>&1

*Output:* 
Saving file '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg.webp.lossless.webp'
File:      [doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]10[doc-root]o_warynskim_01-400x303.jpg
Dimension: 400 x 303
Output:    100986 bytes
Lossless-ARGB compressed size: 100986 bytes
  * Header size: 2744 bytes, image data size: 98217
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=3 transform=3 cache=0

Success
Reduction: -182% (went from 35 kb to 99 kb)

Picking lossy
cwebp succeeded :)

Converted image in 977 ms, reducing file size with 37% (went from 35 kb to 22 kb)
