WebP Express 0.17.2. Conversion triggered using bulk conversion, 2019-12-20 13:30:49

*WebP Convert 2.3.0*  ignited.
- PHP version: 7.3.1
- Server software: Apache

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]12[doc-root]3-3-1536x1086.jpg
- destination: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]12[doc-root]3-3-1536x1086.jpg.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "none"
- near-lossless: 60
- quality: "auto"
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]12[doc-root]3-3-1536x1086.jpg
- destination: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]12[doc-root]3-3-1536x1086.jpg.webp
- default-quality: 70
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- max-quality: 80
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: "auto"
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version
*Warning: PHP Startup: Unable to fork [cwebp -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 


*Warning: PHP Startup: Unable to fork [cwebp -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 

. Result: *Exec failed* (return code: -1)
Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)

*Warning: PHP Startup: Unable to fork [which -a cwebp] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Helpers[doc-root]BinaryDiscovery.php, line 77, PHP 7.3.1 (Linux)* 


*Warning: PHP Startup: Unable to fork [which -a cwebp] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Helpers[doc-root]BinaryDiscovery.php, line 77, PHP 7.3.1 (Linux)* 


*Warning: PHP Startup: Unable to fork [whereis -b cwebp] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Helpers[doc-root]BinaryDiscovery.php, line 55, PHP 7.3.1 (Linux)* 


*Warning: PHP Startup: Unable to fork [whereis -b cwebp] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Helpers[doc-root]BinaryDiscovery.php, line 55, PHP 7.3.1 (Linux)* 

Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Linux)... We do. We in fact have 3
Found 3 binaries: 
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64
Detecting versions of the cwebp binaries found
- Executing: [doc-root]usr[doc-root]bin[doc-root]cwebp -version
*Warning: PHP Startup: Unable to fork [[doc-root]usr[doc-root]bin[doc-root]cwebp -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 


*Warning: PHP Startup: Unable to fork [[doc-root]usr[doc-root]bin[doc-root]cwebp -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 

. Result: *Exec failed* (return code: -1)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64 -version
*Warning: PHP Startup: Unable to fork [[doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64 -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 


*Warning: PHP Startup: Unable to fork [[doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64 -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 

. Result: *Exec failed* (return code: -1)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static -version
*Warning: PHP Startup: Unable to fork [[doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 


*Warning: PHP Startup: Unable to fork [[doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 

. Result: *Exec failed* (return code: -1)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64 -version
*Warning: PHP Startup: Unable to fork [[doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64 -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 


*Warning: PHP Startup: Unable to fork [[doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64 -version] in [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Cwebp.php, line 420, PHP 7.3.1 (Linux)* 

. Result: *Exec failed* (return code: -1)

**Error: ** **None of the cwebp files can be executed (failure code: -1)** 
None of the cwebp files can be executed (failure code: -1)
cwebp failed in 58 ms

*Trying: imagick* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]12[doc-root]3-3-1536x1086.jpg
- destination: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2019[doc-root]12[doc-root]3-3-1536x1086.jpg.webp
- default-quality: 70
- encoding: "auto"
- log-call-arguments: true
- max-quality: 80
- metadata: "none"
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- low-memory: false
- method: 6
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- near-lossless
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Quality of source is 82. This is higher than max-quality, so using max-quality instead (80)
Reduction: 63% (went from 176 kb to 65 kb)

Converting to lossless
Reduction: -117% (went from 176 kb to 382 kb)

Picking lossy
imagick succeeded :)

Converted image in 1610 ms, reducing file size with 63% (went from 176 kb to 65 kb)
