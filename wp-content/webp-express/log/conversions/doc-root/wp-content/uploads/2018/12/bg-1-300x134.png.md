WebP Express 0.17.2. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2019-11-30 14:46:37

*WebP Convert 2.3.0*  ignited.
- PHP version: 7.3.1
- Server software: Apache

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png
- destination: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png
- destination: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: version: *0.5.2*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 3 binaries: 
- [doc-root]usr[doc-root]sbin[doc-root]cwebp
- [doc-root]bin[doc-root]cwebp
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Linux)... We do. We in fact have 3
Found 3 binaries: 
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64
Detecting versions of the cwebp binaries found
- Executing: cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]sbin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]sbin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]bin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]bin[doc-root]cwebp: (version: 0.5.2)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 0.5.2
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice cwebp -metadata none -q 85 -alpha_q '85' -m 6 -low_memory '[doc-root][doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png' -o '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png.webp.lossy.webp' 2>&1

*Output:* 
Saving file '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png.webp.lossy.webp'
File:      [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png
Dimension: 300 x 134 (with alpha)
Output:    6806 bytes Y-U-V-All-PSNR 99.00 99.00 99.00   99.00 dB
block count:  intra4: 0
              intra16: 171  (-> 100.00%)
              skipped block: 170 (99.42%)
bytes used:  header:             22  (0.3%)
             mode-partition:     91  (1.3%)
             transparency:     6630 (99.0 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |       0 |       0 |       0 |       0 |       0  (0.0%)
 intra16-coeffs:  |       4 |       0 |       0 |       0 |       4  (0.1%)
  chroma coeffs:  |       0 |       0 |       0 |       0 |       0  (0.0%)
    macroblocks:  |      15%|       0%|       0%|      84%|     171
      quantizer:  |      20 |      19 |      16 |      12 |
   filter level:  |       7 |       4 |       3 |       2 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |       4 |       0 |       0 |       0 |       4  (0.1%)
Lossless-alpha compressed size: 6629 bytes
  * Header size: 72 bytes, image data size: 6557
  * Lossless features used: PALETTE
  * Precision Bits: histogram=3 transform=3 cache=0
  * Palette size:   126

Success
Reduction: 31% (went from 9887 bytes to 6806 bytes)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: version: *0.5.2*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 3 binaries: 
- [doc-root]usr[doc-root]sbin[doc-root]cwebp
- [doc-root]bin[doc-root]cwebp
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Linux)... We do. We in fact have 3
Found 3 binaries: 
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64
Detecting versions of the cwebp binaries found
- Executing: cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]sbin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]sbin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]bin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]bin[doc-root]cwebp: (version: 0.5.2)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 0.5.2
Trying to convert by executing the following command:
nice cwebp -metadata none -q 85 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root][doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png' -o '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png.webp.lossless.webp' 2>&1

*Output:* 
Saving file '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png.webp.lossless.webp'
File:      [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]2018[doc-root]12[doc-root]bg-1-300x134.png
Dimension: 300 x 134
Output:    6660 bytes
Lossless-ARGB compressed size: 6660 bytes
  * Header size: 78 bytes, image data size: 6557
  * Lossless features used: PALETTE
  * Precision Bits: histogram=3 transform=3 cache=0
  * Palette size:   126

Success
Reduction: 33% (went from 9887 bytes to 6660 bytes)

Picking lossless
cwebp succeeded :)

Converted image in 300 ms, reducing file size with 33% (went from 9887 bytes to 6660 bytes)
