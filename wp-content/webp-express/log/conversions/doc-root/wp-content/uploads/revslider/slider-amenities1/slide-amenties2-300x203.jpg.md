WebP Express 0.17.2. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2019-11-30 14:46:30

*WebP Convert 2.3.0*  ignited.
- PHP version: 7.3.1
- Server software: Apache

Stack converter ignited
Destination folder does not exist. Creating folder: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg
- destination: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "none"
- near-lossless: 60
- quality: "auto"
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg
- destination: [doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg.webp
- default-quality: 70
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- max-quality: 80
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: "auto"
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: version: *0.5.2*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 3 binaries: 
- [doc-root]usr[doc-root]sbin[doc-root]cwebp
- [doc-root]bin[doc-root]cwebp
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Linux)... We do. We in fact have 3
Found 3 binaries: 
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64
Detecting versions of the cwebp binaries found
- Executing: cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]sbin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]sbin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]bin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]bin[doc-root]cwebp: (version: 0.5.2)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 0.5.2
Quality of source is 82. This is higher than max-quality, so using max-quality instead (80)
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
nice cwebp -metadata none -q 80 -alpha_q '85' -m 6 -low_memory '[doc-root][doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg' -o '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg.webp.lossy.webp' 2>&1

*Output:* 
Saving file '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg.webp.lossy.webp'
File:      [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg
Dimension: 300 x 203
Output:    13642 bytes Y-U-V-All-PSNR 39.70 43.93 44.26   40.73 dB
block count:  intra4: 233
              intra16: 14  (-> 5.67%)
              skipped block: 0 (0.00%)
bytes used:  header:            183  (1.3%)
             mode-partition:   1209  (8.9%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |   10458 |      73 |      13 |      35 |   10579  (77.5%)
 intra16-coeffs:  |      79 |      50 |      24 |      11 |     164  (1.2%)
  chroma coeffs:  |    1438 |      18 |      12 |      13 |    1481  (10.9%)
    macroblocks:  |      92%|       3%|       1%|       2%|     247
      quantizer:  |      20 |      15 |      11 |      11 |
   filter level:  |       7 |       3 |       2 |       0 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |   11975 |     141 |      49 |      59 |   12224  (89.6%)

Success
Reduction: 23% (went from 17 kb to 13 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version. Result: version: *0.5.2*
We could get the version, so yes, a plain cwebp call works
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 3 binaries: 
- [doc-root]usr[doc-root]sbin[doc-root]cwebp
- [doc-root]bin[doc-root]cwebp
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 1 binaries: 
- [doc-root]usr[doc-root]bin[doc-root]cwebp
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (Linux)... We do. We in fact have 3
Found 3 binaries: 
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static
- [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64
Detecting versions of the cwebp binaries found
- Executing: cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]sbin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]usr[doc-root]bin[doc-root]cwebp -version. Result: version: *0.5.2*
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-103-linux-x86-64-static -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
- Executing: [doc-root]wp-content[doc-root]plugins[doc-root]webp-express[doc-root]vendor[doc-root]rosell-dk[doc-root]webp-convert[doc-root]src[doc-root]Convert[doc-root]Converters[doc-root]Binaries[doc-root]cwebp-061-linux-x86-64 -version. Result: *Exec failed*. Permission denied (the user that the command was run with does not have permission to execute that binary)
Binaries ordered by version number.
- cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]sbin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]bin[doc-root]cwebp: (version: 0.5.2)
- [doc-root]usr[doc-root]bin[doc-root]cwebp: (version: 0.5.2)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 0.5.2
Trying to convert by executing the following command:
nice cwebp -metadata none -q 80 -alpha_q '85' -near_lossless 60 -m 6 -low_memory '[doc-root][doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg' -o '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg.webp.lossless.webp' 2>&1

*Output:* 
Saving file '[doc-root][doc-root]wp-content[doc-root]webp-express[doc-root]webp-images[doc-root]doc-root[doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg.webp.lossless.webp'
File:      [doc-root][doc-root]wp-content[doc-root]uploads[doc-root]revslider[doc-root]slider-amenities1[doc-root]slide-amenties2-300x203.jpg
Dimension: 300 x 203
Output:    51034 bytes
Lossless-ARGB compressed size: 51034 bytes
  * Header size: 1440 bytes, image data size: 49569
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=3 transform=3 cache=0

Success
Reduction: -188% (went from 17 kb to 50 kb)

Picking lossy
cwebp succeeded :)

Converted image in 478 ms, reducing file size with 23% (went from 17 kb to 13 kb)
