<?php
/* 
 *
 *  Apartments search module
 * 
 */
?>

<?php
$availability_marks = array();

$args_atolgdansk_meters_comm = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'lokal-uzytkowy',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_meters',
            'type'          => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 12
        ),
    ),
    'orderby' => array(
        'atolgdansk_meters' => 'ASC'
    ),
);
$args_atolgdansk_floor_comm = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'lokal-uzytkowy',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_floor',
            'type'          => 'NUMERIC',
            'type' => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 12
        ),
    ),
    'orderby' => array(
        'atolgdansk_floor' => 'ASC'
    ),
);


$args_atolgdansk_exposition_comm = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'lokal-uzytkowy',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_exposition',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 12
        ),
    ),
    'orderby' => array(
        'atolgdansk_exposition' => 'ASC'
    ),
);
$args_atolgdansk_status_comm = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'lokal-uzytkowy',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_status',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 12
        ),
    ),
    'orderby' => array(
        'atolgdansk_status' => 'ASC'
    ),
);

$args_atolgdansk_proptype_comm = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'lokal-uzytkowy',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 12
        ),
    ),
    'orderby' => array(
        'atolgdansk_status' => 'ASC'
    ),
);

$args_atolgdansk_rooms = array(
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'apartament',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_rooms',
            'type'          => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 11
        ),
    ),
    'orderby' => array(
        'atolgdansk_rooms' => 'ASC'
    )
);
$args_atolgdansk_meters = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'apartament',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_meters',
            'type'          => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 11
        ),
    ),
    'orderby' => array(
        'atolgdansk_meters' => 'ASC'
    ),
);
$args_atolgdansk_floor = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'apartament',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_floor',
            'type'          => 'NUMERIC',
            'type' => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 11
        ),
    ),
    'orderby' => array(
        'atolgdansk_floor' => 'ASC'
    ),
);
$args_atolgdansk_exposition = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'apartament',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_exposition',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 11
        ),
    ),
    'orderby' => array(
        'atolgdansk_exposition' => 'ASC'
    ),
);
$args_atolgdansk_status = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'apartament',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_status',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 11
        ),
    ),
    'orderby' => array(
        'atolgdansk_status' => 'ASC'
    ),
);

$args_atolgdansk_proptype = array(
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'apartament',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',    
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 11
        ),
    ),
    'orderby' => array(
        'atolgdansk_status' => 'ASC'
    ),
);

$args_apartment = array(
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'apartament',
    ),
    'orderby' => array(
        'atolgdansk_floor' => 'ASC',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_rooms',
            'type'          => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_meters',
            'type'          => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_floor',
            'type'          => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_exposition',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 11
        ),

    )
);
$args_commercial = array(
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'post_type'        => 'osf_property',
    'tax_query' => array(
        'taxonomy' => 'osf_property_category',
        'field' => 'slug',
        'terms' => 'lokal-uzytkowy',
    ),
    'orderby' => array(
        'atolgdansk_floor' => 'ASC',
    ),
    'meta_query'    => array(
        'relation'        => 'AND',
        array(
            'key'        => 'atolgdansk_meters',
            'type'          => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_floor',
            'type'          => 'NUMERIC',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_exposition',
            'compare'    => '='
        ),
        array(
            'key'        => 'atolgdansk_property_type',
            'compare'    => '=',
            'value'      => 12
        ),

    )
);



// query
$the_query_apartment = new WP_Query($args_apartment);
$the_query_atolgdansk_rooms = new WP_Query($args_atolgdansk_rooms);
$the_query_atolgdansk_meters = new WP_Query($args_atolgdansk_meters);
$the_query_atolgdansk_floor = new WP_Query($args_atolgdansk_floor);
$the_query_atolgdansk_exposition = new WP_Query($args_atolgdansk_exposition);
$the_query_atolgdansk_status = new WP_Query($args_atolgdansk_status);
$the_query_atolgdansk_proptype = new WP_Query($args_atolgdansk_proptype);

$the_query_commercial = new WP_Query($args_commercial);
$the_query_atolgdansk_meters_comm = new WP_Query($args_atolgdansk_meters_comm);
$the_query_atolgdansk_floor_comm = new WP_Query($args_atolgdansk_floor_comm);
$the_query_atolgdansk_exposition_comm = new WP_Query($args_atolgdansk_exposition_comm);
$the_query_atolgdansk_status_comm = new WP_Query($args_atolgdansk_status_comm);
$the_query_atolgdansk_proptype_comm = new WP_Query($args_atolgdansk_proptype_comm);
?>
<?php if ($the_query_apartment->have_posts()) : ?>


<div class="elementor-element elementor-element-ae0ac4e elementor-tabs-view-horizontal elementor-widget elementor-widget-opal-tabs elementor-widget-tabs content__tabs--atolgdansk"
    data-element_type="widget" data-widget_type="opal-tabs.default">
    <div class="elementor-widget-container">
        <div class="elementor-tabs" role="tablist">
            <div class="elementor-tabs-wrapper">
                <div id="elementor-tab-title-1821"
                    class="elementor-tab-title elementor-tab-desktop-title elementor-repeater-item-dc0aa0e elementor-active content__tabs--atolgdansk-apartmenttab"
                    data-tab="1" tabindex="1821" role="tab" aria-controls="elementor-tab-content-1821">
                    <?php _e('Flats', 'atolgdansk') ?></div>


                <?php if ($the_query_commercial->have_posts()) : ?>
                <div id="elementor-tab-title-1822"
                    class="elementor-tab-title elementor-tab-desktop-title  elementor-repeater-item-5a18a25 content__tabs--atolgdansk-commercialtab"
                    data-tab="2" tabindex="1822" role="tab" aria-controls="elementor-tab-content-1822">
                    <?php _e('Commercial units', 'atolgdansk') ?>
                </div>
                <?php endif; ?>
            </div>
            <div class="elementor-tabs-content-wrapper">
                <div id="elementor-tab-content-1821"
                    class="content__tabs--atolgdansk-apartmenttab elementor-tab-content elementor-clearfix elementor-repeater-item-dc0aa0e elementor-active"
                    data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-1821" style="display: block;">

                    <section class="content__wrapper--form-searchbar content__tabs--atolgdansk-apartmenttab">
                        <form class="content__form--searchbar content__form--searchbar-apartament" action=""
                            method="get">
                            <formgroup class="content__formgroup--filter" id="selectgroup">

                                <?php $atolgdans_apart_val = array(); ?>
                                <select data-placeholder="<?php esc_html_e('Rooms', 'atolgdansk'); ?>"
                                    class="content__select content__select--filter-atolgdansk_rooms">
                                    <option></option>
                                    <?php while ($the_query_atolgdansk_rooms->have_posts()) : $the_query_atolgdansk_rooms->the_post(); ?>
                                    <?php
                                                    $field = get_field('atolgdansk_rooms');
                                                    $value = $field['value'];
                                                    $label = $field['label'];
                                                    // stop adding when value was previously present
                                                    if (in_array($value, $atolgdans_apart_val)) {
                                                        continue;
                                                    }
                                                    // add when value wasn't present
                                                    $atolgdans_apart_val[] = $value;

                                                    ?>
                                    <option data-post-id="<?php the_ID(); ?>" value="<?php echo $value; ?>"
                                        class="content__select_option">
                                        <?php _e($label, 'atolgdansk'); ?>
                                    </option>

                                    <?php endwhile; ?>

                                </select>


                                <?php $atolgdans_area_val = array(); ?>
                                <select data-placeholder="<?php esc_html_e('Area', 'atolgdansk'); ?>"
                                    class="content__select content__select--filter-atolgdansk_meters">
                                    <option></option>
                                    <?php while ($the_query_atolgdansk_meters->have_posts()) : $the_query_atolgdansk_meters->the_post(); ?>
                                    <?php
                                                    $field = get_field_object('atolgdansk_meters');
                                                    $value = $field['value'];
                                                    //$label = $field['choices'][$value];
                                                    // stop adding when value was previously present
                                                    if (in_array($value, $atolgdans_area_val)) {
                                                        continue;
                                                    }
                                                    // add when value wasn't present
                                                    $atolgdans_area_val[] = $value;
                                                    ?>



                                    <?php endwhile; ?>
                                    <option value="25" class="content__select_option">
                                        <?php _e('25-45 sq m', 'atolgdansk'); ?></option>
                                    <option value="46" class="content__select_option">
                                        <?php _e('46-65 sq m', 'atolgdansk'); ?></option>
                                    <option value="66" class="content__select_option">
                                        <?php _e('Over 66 sq m', 'atolgdansk'); ?></option>
                                </select>

                                <?php $atolgdans_floor_val = array(); ?>
                                <select data-placeholder="<?php esc_html_e('Floor', 'atolgdansk'); ?>"
                                    class="content__select content__select--filter-atolgdansk_floor">
                                    <option></option>
                                    <?php while ($the_query_atolgdansk_floor->have_posts()) : $the_query_atolgdansk_floor->the_post(); ?>


                                    <?php
                                                    $field = get_field('atolgdansk_floor');
                                                    $value = $field['value'];
                                                    $label = $field['label'];
                                                    // stop adding when value was previously present
                                                    if (in_array($value, $atolgdans_floor_val)) {
                                                        continue;
                                                    }
                                                    // add when value wasn't present
                                                    $atolgdans_floor_val[] = $value;
                                                    ?><option data-post-id="<?php the_ID(); ?>"
                                        value="<?php echo $value; ?>" class="content__select_option">
                                        <?php _e($label, 'atolgdansk'); ?>
                                    </option>

                                    <?php endwhile; ?>
                                </select>

                                <?php $atolgdans_exposition_val = array(); ?>
                                <select data-placeholder="<?php esc_html_e('Cardinal directions', 'atolgdansk'); ?>"
                                    class="content__select content__select--filter-atolgdansk_exposition">
                                    <option></option>
                                    <?php while ($the_query_atolgdansk_exposition->have_posts()) : $the_query_atolgdansk_exposition->the_post(); ?>
                                    <?php $field = get_field('atolgdansk_exposition');
                                                    $value = $field['value'];
                                                    $label = $field['label'];
                                                    // stop adding when value was previously present
                                                    if (in_array($value, $atolgdans_exposition_val)) {
                                                        continue;
                                                    }
                                                    // add when value wasn't present
                                                    $atolgdans_exposition_val[] = $value;
                                                    ?><option data-post-id="<?php the_ID(); ?>"
                                        value="<?php echo $value; ?>" class="content__select_option">
                                        <?php _e($label, 'atolgdansk'); ?>
                                    </option>
                                    <?php endwhile; ?>

                                </select>
                                <button class="content__button--submit content__button--submit-filter"
                                    type="submit"><?php _e('Filter', 'atolgdansk'); ?></button>
                            </formgroup>
                        </form>
                    </section>

                    <section class="content__section--search-results" style="display: none;">
                        <div class="elementor-element elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                            data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div class="elementor-element elementor-column elementor-col-100 elementor-top-column"
                                        data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-heading__align-left elementor-widget elementor-widget-heading"
                                                    data-element_type="widget" data-widget_type="heading.default">
                                                    <div class="elementor-widget-container">
                                                        <i class="icon_before opal-icon-decor " aria-hidden="true"></i>
                                                        <p class="content__heading--results">
                                                            <?php _e('SEARCH RESULTS', 'atolgdansk'); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element  elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                            data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div class="elementor-element elementor-column elementor-col-100 elementor-top-column"
                                        data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-widget overflow-container"
                                                    data-element_type="widget" data-widget_type="heading.default">

                                                    <table class="table content__table--search-results">
                                                        <thead>
                                                            <tr class="property-variation-item">
                                                                <th> <?php _e('Type', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Number of Rooms', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Area', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Floor', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Exposition', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Status', 'atolgdansk'); ?></th>
                                                                <!-- <th> <?php _e('Parking Spot', 'atolgdansk'); ?></th> -->
                                                                <th> <?php _e('Flat Plan', 'atolgdansk'); ?></th>
                                                                <th style="text-align: center;">
                                                                    <?php _e('3D View', 'atolgdansk'); ?></th>
                                                                <!-- <th> Rzut mieszkania</th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php while ($the_query_apartment->have_posts()) : $the_query_apartment->the_post(); ?>

                                                            <?php
                                                                            $fields = get_fields();
                                                                            
                                                                            if ($fields) :

                                                                                if (!$fields['atolgdansk_status']['label']) {
                                                                                    $fields['atolgdansk_status']['label'] = '-';
                                                                                }
                                                                            

                                                                                $status_css_bg_color = '#f4c07a'; // Dostępne
                                                                                $status_css_txt_color = '#014364 !important';

                                                                                $apartment_type_replace = str_replace('/', '-', $fields['atolgdansk_type']);
                                                                                $apartment_type_replace = str_replace('   ', '-', $apartment_type_replace);
                                                                                $apartment_type_replace = str_replace('  ', '-', $apartment_type_replace);
                                                                                $apartment_type_replace = strtolower(str_replace(' ', '-', $apartment_type_replace));

                                                                                if ($fields['atolgdansk_status']['value'] == '0') { // Dostępne
                                                                                    $status_css_bg_color = '#f4c07a !important';
                                                                                    $status_css_txt_color = '#014364 !important';
                                                                                } else if ($fields['atolgdansk_status']['value'] == '1') { // Zarezerwowane
                                                                                    $status_css_bg_color = '#b3c1cb !important';
                                                                                    $status_css_txt_color = '#014364 !important';
                                                                                } else if ($fields['atolgdansk_status']['value'] == '2') { // Sprzedane
                                                                                    $status_css_bg_color = '#014364 !important';
                                                                                    $status_css_txt_color = '#ffffff !important';
                                                                                }

                                                                                $apartment_type_bg_cssid = '#status-layer-' . $apartment_type_replace  . '-fill';
                                                                                $apartment_type_txt_cssid = '#text-layer-' . $apartment_type_replace  . '-fill';
                                                                                
                                                                                $availability_marks[] = array($apartment_type_bg_cssid, $status_css_bg_color);
                                                                                $availability_marks[] = array($apartment_type_txt_cssid, $status_css_txt_color);

                                                                                $img_link = wp_get_attachment_url(apply_filters( 'wpml_object_id', $fields['atolgdansk_plan_view'], 'attachment', TRUE  ));
                                                                                
                                                                                echo '<tr class="property-variation-item">';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . __($fields['atolgdansk_type'], 'atolgdansk') . '</td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . __($fields['atolgdansk_rooms']['label'], 'atolgdansk') . '</span></td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . $fields['atolgdansk_meters'] . ' ' . __('sq m','atolgdansk') . '</span></td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . __($fields['atolgdansk_floor']['label'], 'atolgdansk') . '</span></td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . __($fields['atolgdansk_exposition']['label'], 'atolgdansk') . '</span></td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . __($fields['atolgdansk_status']['label'], 'atolgdansk') . '</span></td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span><a download href="' . $img_link . '" class="button-primary property_variation_button" role="button">' . __('Download','atolgdansk') . '</a></span></td>';
                                                                                echo '<td class="table__search--results-apartment-type" style="text-align: center;"><span>' . __($fields['virtual_walk'], 'atolgdansk') . '</span></td>';
                                                                                //echo '<td class="table__search--results-apartment-type"><span><a data-elementor-open-lightbox="yes" href="' . $fields['atolgdansk_plan_view'] . '" class="button-primary property_variation_button" role="button">Zobacz</a></span></td>';
                                                                                //echo '<td class="table__search--results-apartment-type"><span>' . $fields['atolgdansk_isometric_view'] . '</span></td>';
                                                                                //echo '<td class="table__search--results-apartment-type"><span>' . $fields['atolgdansk_parking_spot'] . '</span></td>';
                                                                                //echo '<td class="table__search--results-apartment-type"><span>' . $fields['atolgdansk_parking_spot_amount'] . '</span></td>';
                                                                                echo '</tr>';

                                                                            endif; ?>

                                                            <?php endwhile; ?>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                </div>


                <div id="elementor-tab-content-1822"
                    class="content__tabs--atolgdansk-commercialtab elementor-tab-content elementor-clearfix  elementor-repeater-item-5a18a25"
                    data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-1822" style="display: none;">

                    <section class="content__wrapper--form-searchbar">
                        <form class="content__form--searchbar content__form--searchbar-commercial" action=""
                            method="get">
                            <formgroup class="content__formgroup--filter" id="selectgroup">

                                <?php $atolgdans_area_val = array(); ?>
                                <select data-placeholder="<?php esc_html_e('Area', 'atolgdansk'); ?>"
                                    class="content__select content__select--filter-atolgdansk_meters">
                                    <option></option>
                                    <?php while ($the_query_atolgdansk_meters_comm->have_posts()) : $the_query_atolgdansk_meters_comm->the_post(); ?>
                                    <?php
                                                    $field = get_field_object('atolgdansk_meters');
                                                    $value = $field['value'];
                                                    //$label = $field['choices'][$value];
                                                    // stop adding when value was previously present
                                                    if (in_array($value, $atolgdans_area_val)) {
                                                        continue;
                                                    }
                                                    // add when value wasn't present
                                                    $atolgdans_area_val[] = $value;
                                                    ?>



                                    <?php endwhile; ?>
                                    <option value="50" class="content__select_option">
                                        <?php _e('50-120 sq m', 'atolgdansk'); ?></option>
                                    <option value="121" class="content__select_option">
                                        <?php _e('Over 121 sq m', 'atolgdansk'); ?></option>
                                </select>

                                <?php $atolgdans_floor_val = array(); ?>
                                <select data-placeholder="<?php esc_html_e('Floor', 'atolgdansk'); ?>"
                                    class="content__select content__select--filter-atolgdansk_floor">
                                    <option></option>
                                    <?php while ($the_query_atolgdansk_floor_comm->have_posts()) : $the_query_atolgdansk_floor_comm->the_post(); ?>


                                    <?php
                                                    $field = get_field('atolgdansk_floor');
                                                    $value = $field['value'];
                                                    $label = $field['label'];
                                                    // stop adding when value was previously present
                                                    if (in_array($value, $atolgdans_floor_val)) {
                                                        continue;
                                                    }
                                                    // add when value wasn't present
                                                    $atolgdans_floor_val[] = $value;
                                                    ?><option data-post-id="<?php the_ID(); ?>"
                                        value="<?php echo $value; ?>" class="content__select_option">
                                        <?php _e($label, 'atolgdansk'); ?>
                                    </option>

                                    <?php endwhile; ?>
                                </select>

                                <?php $atolgdans_exposition_val = array(); ?>
                                <select data-placeholder="<?php esc_html_e('Cardinal directions', 'atolgdansk'); ?>"
                                    class="content__select content__select--filter-atolgdansk_exposition">
                                    <option></option>
                                    <?php while ($the_query_atolgdansk_exposition_comm->have_posts()) : $the_query_atolgdansk_exposition_comm->the_post(); ?>
                                    <?php $field = get_field('atolgdansk_exposition');
                                                    $value = $field['value'];
                                                    $label = $field['label'];
                                                    // stop adding when value was previously present
                                                    if (in_array($value, $atolgdans_exposition_val)) {
                                                        continue;
                                                    }
                                                    // add when value wasn't present
                                                    $atolgdans_exposition_val[] = $value;
                                                    ?><option data-post-id="<?php the_ID(); ?>"
                                        value="<?php echo $value; ?>" class="content__select_option">
                                        <?php _e($label, 'atolgdansk'); ?>
                                    </option>
                                    <?php endwhile; ?>

                                </select>
                                <button class="content__button--submit content__button--submit-filter"
                                    type="submit"><?php _e('Filter', 'atolgdansk'); ?></button>
                            </formgroup>
                        </form>
                    </section>

                    <section class="content__section--search-results" style="display: none;">
                        <div class="elementor-element elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                            data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div class="elementor-element elementor-column elementor-col-100 elementor-top-column"
                                        data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-heading__align-left elementor-widget elementor-widget-heading"
                                                    data-element_type="widget" data-widget_type="heading.default">
                                                    <div class="elementor-widget-container">
                                                        <i class="icon_before opal-icon-decor " aria-hidden="true"></i>
                                                        <p class="content__heading--results">
                                                            <?php _e('SEARCH RESULTS', 'atolgdansk'); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="elementor-element  elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                            data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div class="elementor-element elementor-column elementor-col-100 elementor-top-column"
                                        data-element_type="column">
                                        <div class="elementor-column-wrap  elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-widget overflow-container"
                                                    data-element_type="widget" data-widget_type="heading.default">

                                                    <table class="table content__table--search-results">
                                                        <thead>
                                                            <tr class="property-variation-item">
                                                                <th> <?php _e('Type', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Area', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Floor', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Exposition', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Status', 'atolgdansk'); ?></th>
                                                                <th> <?php _e('Flat plan', 'atolgdansk'); ?></th>
                                                                <th style="text-align: center;">
                                                                    <?php _e('3D View', 'atolgdansk'); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php while ($the_query_commercial->have_posts()) : $the_query_commercial->the_post(); ?>

                                                            <?php
                                                                            $fields = get_fields();
                                                                            if ($fields) :

                                                                                if (!$fields['atolgdansk_status']['label']) {
                                                                                    $fields['atolgdansk_status']['label'] = '-';
                                                                                }


                                                                                $status_css_bg_color = '#f4c07a'; // Dostępne
                                                                                $status_css_txt_color = '#014364 !important';

                                                                                $apartment_type_replace = str_replace('/', '-', $fields['atolgdansk_type']);
                                                                                $apartment_type_replace = str_replace('   ', '-', $apartment_type_replace);
                                                                                $apartment_type_replace = str_replace('  ', '-', $apartment_type_replace);
                                                                                $apartment_type_replace = strtolower(str_replace(' ', '-', $apartment_type_replace));

                                                                                if ($fields['atolgdansk_status']['value'] == '0') { // Dostępne
                                                                                    $status_css_bg_color = '#f4c07a !important';
                                                                                    $status_css_txt_color = '#014364 !important';
                                                                                } else if ($fields['atolgdansk_status']['value'] == '1') { // Zarezerwowane
                                                                                    $status_css_bg_color = '#b3c1cb !important';
                                                                                    $status_css_txt_color = '#014364 !important';
                                                                                } else if ($fields['atolgdansk_status']['value'] == '2') { // Sprzedane
                                                                                    $status_css_bg_color = '#014364 !important';
                                                                                    $status_css_txt_color = '#f4c07a !important';
                                                                                }

                                                                                $apartment_type_bg_cssid = '#status-layer-' . $apartment_type_replace  . '-fill';
                                                                                $apartment_type_txt_cssid = '#text-layer-' . $apartment_type_replace  . '-fill';
                                                                                
                                                                                $availability_marks[] = array($apartment_type_bg_cssid, $status_css_bg_color);
                                                                                $availability_marks[] = array($apartment_type_txt_cssid, $status_css_txt_color);
                                                                                
                                                                                $img_link = wp_get_attachment_url(apply_filters( 'wpml_object_id', $fields['atolgdansk_plan_view'], 'attachment', TRUE  ));

                                                                                echo '<tr class="property-variation-item">';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . __($fields['atolgdansk_type'], 'atolgdansk') . '</td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . $fields['atolgdansk_meters'] . ' ' . __('sq m', 'atolgdansk') . '</span></td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . __($fields['atolgdansk_floor']['label'], 'atolgdansk') . '</span></td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . __($fields['atolgdansk_exposition']['label'], 'atolgdansk') . '</span></td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span>' . __($fields['atolgdansk_status']['label'], 'atolgdansk') . '</span></td>';
                                                                                echo '<td class="table__search--results-apartment-type"><span><a download href="' . $img_link . '" class="button-primary property_variation_button" role="button">' . __('Download','atolgdansk') . '</a></span></td>';
                                                                                echo '<td class="table__search--results-apartment-type" style="text-align: center;"><span>' . __($fields['virtual_walk'], 'atolgdansk') . '</span></td>';
                                                                                //echo '<td class="table__search--results-apartment-type"><span><a data-elementor-open-lightbox="yes" href="' . $fields['atolgdansk_plan_view'] . '" class="button-primary property_variation_button" role="button">Zobacz</a></span></td>';
                                                                                echo '</tr>';

                                                                            endif; ?>

                                                            <?php endwhile; ?>
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>


<?php wp_reset_query();


// repeater field: note that the_sub_field and get_sub_field don't need a post id parameter
if( have_rows('atolgdansk_garage', 2363) ): // the_garage_list_page ?>

<?php while( have_rows('atolgdansk_garage', 2363) ): the_row(); ?>

<?php 
        

        $garage_no = get_sub_field('atolgdansk_garage_no');
        
        if (get_sub_field('atolgdansk_status')['value'] == '0') { // Dostępne
            $status_css_bg_color = '#f4c07a !important';
            $status_css_txt_color = '#014364 !important';
        } else if (get_sub_field('atolgdansk_status')['value'] == '1') { // Zarezerwowane
            $status_css_bg_color = '#b3c1cb !important';
            $status_css_txt_color = '#014364 !important';
        } else if (get_sub_field('atolgdansk_status')['value'] == '2') { // Sprzedane
            $status_css_bg_color = '#014364 !important';
            $status_css_txt_color = '#ffffff !important';
        }
        
        $garage_type_bg_cssid = '#status-layer-g-p-' . $garage_no  . '-fill';
        $garage_type_txt_cssid = '#text-layer-g-p-' . $garage_no  . '-fill';
        
        $availability_marks[] = array($garage_type_bg_cssid, $status_css_bg_color);
        $availability_marks[] = array($garage_type_txt_cssid, $status_css_txt_color);
        
        ?>

<?php endwhile; ?>



<?php endif;




//PC::debug ($availability_marks);
echo '<style>';

foreach ($availability_marks as $availability_mark) {
    //PC::debug($availability_mark);
    echo $availability_mark[0] . "{ \r\n fill: " . $availability_mark[1] . "; \r\n }";
}

echo '</style>';