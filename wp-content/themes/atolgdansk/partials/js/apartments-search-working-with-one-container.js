jQuery(document).ready(function($) {
    // Show footer search results section
    $("section.content__section--search-results:eq(2),section.content__section--search-results:eq(3)").show();
    $(".content__tabs--atolgdansk.elementor-widget-opal-tabs .elementor-tab-title").on("click", function() {
        $('.content__tabs--atolgdansk.elementor-widget-opal-tabs .elementor-tab-title').not($(this)).removeClass('elementor-active')
        // The clicked tab same classname elements
        var otherTabElem = $(".content__tabs--atolgdansk.elementor-widget-opal-tabs .elementor-tab-title." + $(this).attr('class').split(" ").pop());
        // Hide all tab content to later on show correct one
        $('.content__tabs--atolgdansk.elementor-widget-opal-tabs .elementor-tab-content').hide()
        var otherCotentElement = $('.content__tabs--atolgdansk.elementor-widget-opal-tabs .elementor-tab-content.' + $(this).attr('class').split(" ").pop())
        // Now dhow correct one
        otherCotentElement.show();
        otherTabElem.each(function() {
            otherTabElem.not($(this)).addClass('elementor-active')
        })
    })
    var stickyMenuHeight = 175;
    // Apply select2 to select tags
    $(".content__formgroup--filter select").each(function() {
        $(this).select2({
            dropdownAutoWidth: false,
            width: "20%",
            minimumResultsForSearch: Infinity,
            placeholder: function() {
                $(this).data("placeholder");
            },
            allowClear: true
        });
        // Close dropdowns when clear is clicked
        $(this).on("select2:clear", function(e) {
            $(this).on("select2:opening.cancelOpen", function(e) {
                e.preventDefault();
                $(this).off("select2:opening.cancelOpen");
                $("select").select2('close');
            });
        });
    });
    // 
    var atolAjaxReqApartments = {
        atolgdansk_proptype: 11
    }
    $(".content__tabs--atolgdansk.elementor-widget-opal-tabs .elementor-tab-title").on("click", function(e) {
        if ($(this).is('.elementor-tab-title.content__tabs--atolgdansk-commercialtab')) {
            atolAjaxReqApartments["atolgdansk_proptype"] = 12 // Lokale uzytkowe term id
        } else if ($(this).is('.elementor-tab-title.content__tabs--atolgdansk-apartmenttab')) {
            atolAjaxReqApartments["atolgdansk_proptype"] = 11 //Apartamenty term id
        }
    });


    function getApartmentData() {


        var triggeringElem = $(this);

        var $currentSelectionOptionsAll = [];
        // Get all currently selected values
        var $currentSelectionAll = [];
        // Get all existing options
        var $currentSelectionOptionsAll = [];
        $(this).closest("formgroup").find("select").each(function() {
            $currentSelectionAll.push($(this).val());
            $(this).find("option").each(function() {
                $currentSelectionOptionsAll.push($(this).val());
            });
        });
        var $thisResultsSection = $(this).closest("section").next("section.content__section--search-results");
        atolAjaxReqApartments = {
            action: "atolgetapartments", // AJAX action for admin-ajax.php
            nonce: atoljs_search_ajax_params.nonce
        };
        var resultTable = ''
        if ($(this).is('.content__tabs--atolgdansk-commercialtab .content__button--submit-filter') || $(this).is('.content__tabs--atolgdansk-commercialtab select')) {
            atolAjaxReqApartments["atolgdansk_proptype"] = 12 // Lokale uzytkowe term id
            if ($currentSelectionAll[0] !== "") atolAjaxReqApartments["atolgdansk_meters"] = $currentSelectionAll[0];
            if ($currentSelectionAll[1] !== "") atolAjaxReqApartments["atolgdansk_floor"] = $currentSelectionAll[1];
            if ($currentSelectionAll[2] !== "") atolAjaxReqApartments["atolgdansk_exposition"] = $currentSelectionAll[2];
            resultTable = $(".content__tabs--atolgdansk-commercialtab .content__table--search-results")
            passTypeId = atolAjaxReqApartments["atolgdansk_proptype"]
        } else if ($(this).is('.content__tabs--atolgdansk-apartmenttab .content__button--submit-filter') || $(this).is('.content__tabs--atolgdansk-apartmenttab select')) {
            atolAjaxReqApartments["atolgdansk_proptype"] = 11 //Apartamenty term id
            if ($currentSelectionAll[0] !== "") atolAjaxReqApartments["atolgdansk_rooms"] = $currentSelectionAll[0];
            if ($currentSelectionAll[1] !== "") atolAjaxReqApartments["atolgdansk_meters"] = $currentSelectionAll[1];
            if ($currentSelectionAll[2] !== "") atolAjaxReqApartments["atolgdansk_floor"] = $currentSelectionAll[2];
            if ($currentSelectionAll[3] !== "") atolAjaxReqApartments["atolgdansk_exposition"] = $currentSelectionAll[3];
            resultTable = $(".content__tabs--atolgdansk-apartmenttab .content__table--search-results")
            passTypeId = atolAjaxReqApartments["atolgdansk_proptype"]
        }
        //console.log(atolAjaxReqApartments["atolgdansk_proptype"])
        var theAjaxCall = $.ajax({
            type: "POST",
            url: atoljs_search_ajax_params.ajaxurl,
            dataType: "json",
            data: atolAjaxReqApartments,
            triggeringElem: triggeringElem,
            success: function(data) {
                if (data) {
                    var roomsNotInCommercial = '';
                    resultTable.each(function() {
                        $(this).find("tbody tr").remove();
                    })
                    var ajaxData = [];
                    var len = data.length;
                    for (var i = 0; i < len; i++) {
                        ajaxData.push(data[i][0]);
                        if (passTypeId == 11) {
                            roomsNotInCommercial = '<td class="table__search--results-apartment-type">' + data[i][0].atolgdansk_rooms.label + "</td>";
                        } else if (passTypeId == 12) {
                            roomsNotInCommercial = ''
                        }
                        if (data[i][0].atolgdansk_plan_view) {
                            data[i][0].atolgdansk_plan_view = '<a data-elementor-open-lightbox="yes" href="' + data[i][0].atolgdansk_plan_view + '" class="button-primary property_variation_button" role="button">Zobacz</a>';
                        } else {
                            data[i][0].atolgdansk_plan_view = "-";
                        }
                        if (data[i][0].atolgdansk_status) {} else {
                            data[i][0].atolgdansk_status = {
                                label: "-"
                            };
                        }
                        tr_str = '<tr class="property-variation-item">' + '<td class="table__search--results-apartment-type">' + data[i][0].atolgdansk_type + "</td>" + roomsNotInCommercial + '<td class="table__search--results-apartment-type">' + data[i][0].atolgdansk_meters + " m<sup>2</sup></td>" + '<td class="table__search--results-apartment-type">' + data[i][0].atolgdansk_floor.label + "</td>" + '<td class="table__search--results-apartment-type">' + data[i][0].atolgdansk_exposition.label + "</td>" + '<td class="table__search--results-apartment-type">' + data[i][0].atolgdansk_status.label + "</td>" +
                            // + '<td class="table__search--results-apartment-type">' + data[i][0].atolgdansk_parking_spot_amount + '</td>'
                            '<td class="table__search--results-apartment-type">' + data[i][0].atolgdansk_plan_view + "</td>" +
                            // + '<td class="table__search--results-apartment-type">' + data[i][0].atolgdansk_isometric_view + '</td>'
                            "</tr>";
                        resultTable.each(function() {
                            $(this).append(tr_str);
                        })
                        $(".content__tabs--atolgdansk:eq(1) section.content__section--search-results").show();
                        if ($(this).not('select')) {
                            $('html, body').stop().animate({
                                scrollTop: $(".content__tabs--atolgdansk:eq(1)").offset().top - stickyMenuHeight
                            }, 1000);
                        }
                    }

                    ajaxData.some(function(item) {
                        
                        $triggeringElem = triggeringElem

                        if (item.atolgdansk_property_type == 12 ) {
                            
                            $triggeringElem.siblings().find('option').not(':selected').each(function() {
                                // var $sameContainerElem = $parentSearchBoxElem;
                                // var $sameClassElem = $(this).attr("class");
                                // var allSameElem = ""+$sameContainerElem + " " + $sameClassElem+"";
                                // console.log(allSameElem)
                                $(this).attr("disabled", true);
                            })
                        }
                        if (item.atolgdansk_property_type == 11 ) {
                            $triggeringElem.siblings().find('option').not(':selected').each(function() {
                                // var $sameContainerElem = $parentSearchBoxElem;
                                // var $sameClassElem = $(this).attr("class");
                                // var allSameElem = ""+$sameContainerElem + " " + $sameClassElem+"";
                                // console.log(allSameElem)
                                $(this).attr("disabled", true);
                            })
                        }

                    });

                    ajaxData.some(function(item) {

                        $triggeringElem = triggeringElem

                        if (item.atolgdansk_property_type == 12 ) {
                            var $parentSearchBoxElem = $(".content__form--searchbar-commercial")
                        } else if (item.atolgdansk_property_type == 11 ) {
                            var $parentSearchBoxElem = $(".content__form--searchbar-apartament")
                        }

                        if (item.atolgdansk_exposition.value === "pln") {
                            $parentSearchBoxElem.find('.content__select--filter-atolgdansk_exposition option[value="pln"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_exposition.value === "pld") {
                            $parentSearchBoxElem.find('.content__select--filter-atolgdansk_exposition option[value="pld"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_exposition.value === "wsch") {
                            $parentSearchBoxElem.find('.content__select--filter-atolgdansk_exposition option[value="wsch"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_exposition.value === "zach") {
                            $parentSearchBoxElem.find('.content__select--filter-atolgdansk_exposition option[value="zach"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_exposition.value === "pld-zach") {
                            $parentSearchBoxElem.find('.content__select--filter-atolgdansk_exposition option[value="pld-zach"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_exposition.value === "pld-wsch") {
                            $parentSearchBoxElem.find('.content__select--filter-atolgdansk_exposition option[value="pld-wsch"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        } else if (item.atolgdansk_exposition.value === "pln-wsch") {
                            $parentSearchBoxElem.find('.content__select--filter-atolgdansk_exposition option[value="pln-wsch"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_exposition.value === "pln-zach") {
                            $parentSearchBoxElem.find('.content__select--filter-atolgdansk_exposition option[value="pln-zach"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }

                        if (item.atolgdansk_floor.value == "0") {
                                $parentSearchBoxElem.find('.content__select--filter-atolgdansk_floor option[value="0"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_floor.value == "1") {
                                $parentSearchBoxElem.find('.content__select--filter-atolgdansk_floor option[value="1"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_floor.value == "2") {
                                $parentSearchBoxElem.find('.content__select--filter-atolgdansk_floor option[value="2"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_floor.value == "3") {
                                $parentSearchBoxElem.find('.content__select--filter-atolgdansk_floor option[value="3"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }
                        if (item.atolgdansk_floor.value == "4") {
                                $parentSearchBoxElem.find('.content__select--filter-atolgdansk_floor option[value="4"]').each(function() {
                                $(this).removeAttr("disabled");
                            })
                        }

                        if (item.atolgdansk_property_type == 12) {
                            if (parseInt(item.atolgdansk_meters) < 121) {
                                    $parentSearchBoxElem.find('.content__select--filter-atolgdansk_meters option[value="50"]').each(function() {
                                    $(this).removeAttr("disabled");
                                })
                            }
                            if (parseInt(item.atolgdansk_meters) >= 121) {

                                    $parentSearchBoxElem.find('.content__select--filter-atolgdansk_meters option[value="121"]').each(function() {
                                    $(this).removeAttr("disabled");

                                })
                            }
                        }
                        if (item.atolgdansk_property_type == 11) {
                            if (parseInt(item.atolgdansk_meters) < 46) {
                                    $parentSearchBoxElem.find('.content__select--filter-atolgdansk_meters option[value="25"]').each(function() {
                                    $(this).removeAttr("disabled");
                                })
                            }
                            if (parseInt(item.atolgdansk_meters) >= 46 && parseInt(item.atolgdansk_meters) < 66) {

                                    $parentSearchBoxElem.find('.content__select--filter-atolgdansk_meters option[value="46"]').each(function() {
                                    $(this).removeAttr("disabled");

                                })
                            }
                            if (parseInt(item.atolgdansk_meters) >= 66) {

                                    $parentSearchBoxElem.find('.content__select--filter-atolgdansk_meters option[value="66"]').each(function() {
                                    $(this).removeAttr("disabled");

                                })
                            }

                            if (item.atolgdansk_rooms.value == "1") {
                                    $parentSearchBoxElem.find('.content__select--filter-atolgdansk_rooms option[value="1"]').each(function() {
                                    $(this).removeAttr("disabled");
                                })
                            }

                            if (item.atolgdansk_rooms.value == "2") {
                                    $parentSearchBoxElem.find('.content__select--filter-atolgdansk_rooms option[value="2"]').each(function() {
                                    $(this).removeAttr("disabled");
                                })
                            }

                            if (item.atolgdansk_rooms.value == "3") {
                                    $parentSearchBoxElem.find('.content__select--filter-atolgdansk_rooms option[value="3"]').each(function() {
                                    $(this).removeAttr("disabled");
                                })
                            }

                            if (item.atolgdansk_rooms.value == "4") {
                                    $parentSearchBoxElem.find('.content__select--filter-atolgdansk_rooms option[value="4"]').each(function() {
                                    $(this).removeAttr("disabled");
                                })
                            }
                        }
                    })
                    //console.log(ajaxData);
                }
            }
        }).done(function(data) {
            if (data.length == 0) {
                if ($(this).not('select')) {
                    $('html, body').stop().animate({
                        scrollTop: $(".content__tabs--atolgdansk:eq(1)").offset().top - stickyMenuHeight
                    }, 1000);
                }
                var tr_str = '<tr class="property-variation-item">' + '<td class="blue" colspan="7">Niestety nie znaleziono wyników o wybranych kryteriach. Zmień wartości filtrów w polach wyszukiwania lub <b class="all-apartments"><i class="fa fa-search" aria-hidden="true"></i> zobacz wszystkie lokale i apartamenty</b></td>' + "</tr>";
                resultTable.each(function() {
                    $(this).append(tr_str);
                })
                $(".all-apartments").on("click", function() {
                    $(".content__form--searchbar select").each(function() {
                        $(this).val("").trigger("change");
                    });
                    $(this).closest("section").prev("section").find(".content__button--submit-filter").trigger("click");
                });
            }
        }).fail(function() {}).always(function() {});
        // Set another completion function for the request above
        theAjaxCall.always(function() {});
        //console.log($(this));
    }
    // Trigger search query on click of filter button
    $(".content__button--submit-filter").on("click", getApartmentData)
    // Trigger search query on click of filter button
    $(".content__formgroup--filter select").on("change", getApartmentData)
    $(".content__button--submit-filter").on("click", function(e) {
        e.preventDefault();
    })
    //
    $(".content__formgroup--filter select").each(function() {
        $(this).on("change", function() {
            // This changed select element
            var $thisSelectClass = $(this).attr('class').split(' ')[1];
            var $thisSelectVal = $(this).val();
            // Set any select elem of same class to same value
            var otherSelectElem = $(".content__formgroup--filter select." + $thisSelectClass).not($(this));
            otherSelectElem.val($thisSelectVal).select2('destroy')
            otherSelectElem.select2({
                dropdownAutoWidth: false,
                width: "20%",
                minimumResultsForSearch: Infinity,
                placeholder: function() {
                    $(this).data("placeholder");
                },
                allowClear: true
            })
        });
    });
});