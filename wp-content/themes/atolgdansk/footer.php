</div><!-- #content -->
</div><!-- .site-content-contain -->
<footer id="colophon" class="site-footer">
    <?php
    if (maisonco_is_footer_builder()) {
        maisonco_the_footer_builder();
    } else {
        get_template_part('template-parts/footer');
    }

    do_action('opal-render-footer');
    ?>
</footer><!-- #colophon -->
</div><!-- #page -->
<?php do_action('opal_end_wrapper') ?>
</div><!-- end.opal-wrapper-->
<div id="root3dEstate"></div>
<script>
(function(w, d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
        a = {
            renderDom: function() {}
        };
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.async = !!0;
    js.src = 'https://app.3destate.pl/widget/sdk.js?hash=dad34e489e14ea61771062ab9c3a0ab1';
    fjs.parentNode.insertBefore(js, fjs);
    w.API_3D_ESTATE = a;
}(window, document, 'script', 'jssdk-3destate'));

window.API_3D_ESTATE.renderDom();
</script>
<?php wp_footer(); ?>
</body>

</html>