<?php

/**
 * Child theme functions
 *
 * Functions file for child theme, enqueues parent and child stylesheets by default.
 *
 * @since   1.0.0
 * @package atolgdansk
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}
if (!function_exists('atol_enqueue_styles')) {
    // Add enqueue function to the desired action.
    add_action('wp_enqueue_scripts', 'atol_enqueue_styles');
    /**
     * Enqueue Styles.
     *
     * Enqueue parent style and child styles where parent are the dependency
     * for child styles so that parent styles always get enqueued first.
     *
     * @since 1.0.0
     */
    function atol_enqueue_styles()
    {
        // Parent style variable.
        $parent_style = 'parent-style';
        // Enqueue Parent theme's stylesheet.
        wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
        // Enqueue Child theme's stylesheet.
        // Setting 'parent-style' as a dependency will ensure that the child theme stylesheet loads after it.
        wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));

        //if (is_front_page()) {
            // Ajax script for searchbox

            wp_enqueue_style('apartments-search', get_stylesheet_directory_uri() . '/partials/css/apartments-search.css', array());
            wp_register_style('atolcss-select2', get_stylesheet_directory_uri() . '/partials/css/select2.css', false, '1.0', 'all');
            wp_register_script('atoljs-select2', get_stylesheet_directory_uri() . '/partials/js/select2.js', array('jquery'), '1.0', true);
            wp_register_script('atoljs-apartments-search', get_stylesheet_directory_uri() . '/partials/js/apartments-search.js', array('jquery'), '1.0', true);

            wp_localize_script( 'atoljs-apartments-search', 'atoljs_search_ajax_params', array(
                'ajaxurl' => admin_url( 'admin-ajax.php' ), // WordPress AJAX
                'nonce' => wp_create_nonce('ajax_nonce'),
                //'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
                //'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
                //'max_page' => $wp_query->max_num_pages
             ) );

            wp_enqueue_script('atoljs-apartments-search');
            wp_enqueue_style('atolcss-select2');
            wp_enqueue_script('atoljs-select2');
        //}
    }
}


// add favicon placed in website root
function blog_favicon()
{
echo '<link rel="apple-touch-icon" sizes="180x180" href="' . get_stylesheet_directory_uri() . '/assets/favicon/apple-touch-icon.png">';
echo '<link rel="icon" type="image/png" sizes="32x32" href="' . get_stylesheet_directory_uri() . '/assets/favicon/favicon-32x32.png">';
echo '<link rel="icon" type="image/png" sizes="16x16" href="' . get_stylesheet_directory_uri() . '/assets/favicon/favicon-16x16.png">';
echo '<link rel="manifest" href="' . get_stylesheet_directory_uri() . '/assets/favicon/site.webmanifest">';
echo '<link rel="mask-icon" href="' . get_stylesheet_directory_uri() . '/assets/favicon/safari-pinned-tab.svg" color="#6d9230">';
echo '<link rel="shortcut icon" href="' . get_stylesheet_directory_uri() . '/assets/favicon/favicon.ico">';
echo '<meta name="msapplication-TileColor" content="#ffffff">';
echo '<meta name="msapplication-config" content="' . get_stylesheet_directory_uri() . '/assets/favicon/browserconfig.xml">';
echo '<meta name="theme-color" content="#ffffff">';
}
add_action('wp_head', 'blog_favicon');


add_action( 'wp_ajax_atolgetapartments', 'atol_get_apartment_posts_ajax_callback' ); // wp_ajax_{action}
add_action( 'wp_ajax_nopriv_atolgetapartments', 'atol_get_apartment_posts_ajax_callback' ); // wp_ajax_nopriv_{action} - for lot loggedin users
function atol_get_apartment_posts_ajax_callback() {
    $check_nonce = check_ajax_referer( 'atolgetapartments', 'nonce', false );
    //PC::debug($_POST);
//    if ( !$check_nonce ) {
//        echo 'session_expired';
//        wp_die();
//     }
    $return = array();
    $meta_query = array('relation' => 'AND');

      
    if(isset($_POST['atolgdansk_rooms']) && !empty($_POST['atolgdansk_rooms'])) {
        $atolgdansk_rooms = sanitize_text_field( $_POST['atolgdansk_rooms'] );
        $meta_query[] = array(
            'key' => 'atolgdansk_rooms',
            'value' => $atolgdansk_rooms,
            'compare' => '='
        );
    }
  
    if(isset($_POST['atolgdansk_meters']) && !empty($_POST['atolgdansk_meters'])) {
        $atolgdansk_meters = sanitize_text_field( $_POST['atolgdansk_meters'] );
        if ($atolgdansk_meters == '25') {
            $meta_query[] = array(
                'key' => 'atolgdansk_meters',
                'value' => array(25,45),
                'type' => 'NUMERIC',
                'compare' => 'BETWEEN'
            );
         } else if ($atolgdansk_meters == '46') {
            $meta_query[] = array(
                'key' => 'atolgdansk_meters',
                'value' => array(46,65),
                'type' => 'NUMERIC',
                'compare' => 'BETWEEN'
            );
        } else if ($atolgdansk_meters == '50') {
            $meta_query[] = array(
                'key' => 'atolgdansk_meters',
                'value' => array(50,120),
                'type' => 'NUMERIC',
                'compare' => 'BETWEEN'
            );
        } else if ($atolgdansk_meters == '66') {
            $meta_query[] = array(
                'key' => 'atolgdansk_meters',
                'value' => 66,
                'type' => 'NUMERIC',
                'compare' => '>='
            );
        } else if ($atolgdansk_meters == '121') {
            $meta_query[] = array(
                'key' => 'atolgdansk_meters',
                'value' => 121,
                'type' => 'NUMERIC',
                'compare' => '>='
            );
        }
    }
  
    if(isset($_POST['atolgdansk_floor']) && !empty($_POST['atolgdansk_floor'])) {
        $atolgdansk_floor = sanitize_text_field( $_POST['atolgdansk_floor'] );
        $meta_query[] = array(
            'key' => 'atolgdansk_floor',
            'value' => $atolgdansk_floor,
            'compare' => '='
        );
    }
  
    if(isset($_POST['atolgdansk_exposition']) && !empty($_POST['atolgdansk_exposition'])) {
        $atolgdansk_exposition = sanitize_text_field( $_POST['atolgdansk_exposition'] );
        $meta_query[] = array(
            'key' => 'atolgdansk_exposition',
            'value' => $atolgdansk_exposition,
            'compare' => '='
        );
    }

    if(isset($_POST['atolgdansk_proptype']) && !empty($_POST['atolgdansk_proptype'])) {
        $atolgdansk_proptype = sanitize_text_field( $_POST['atolgdansk_proptype'] );
        $meta_query[] = array(
            'key' => 'atolgdansk_property_type',
            'value' => $atolgdansk_proptype,
            'compare' => '='
        );
    }

    // you can use WP_Query, query_posts() or get_posts() here - it doesn't matter
    $search_results = new WP_Query( array( 
//      's'=> $_POST['q'], // the search query
        'post_status' => 'publish', // if you don't want drafts to be returned
        'posts_per_page' => -1, // how much to show at once
        'post_type'        => 'osf_property',
        'meta_query'    => array(
            'relation'        => 'AND',
            $meta_query
        )
    ) );
    if( $search_results->have_posts() ) :
        while( $search_results->have_posts() ) : $search_results->the_post();   
        //$return[] = array( $search_results->post->ID, $search_results->post, $meta_query ); // 

            $fields = get_fields();

            if ($fields) :

                if (!$fields['atolgdansk_status']['label']) {
                    $fields['atolgdansk_status']['label'] = '-';
                    }

                $fields['atolgdansk_rooms']['label'] = __($fields['atolgdansk_rooms']['label'], 'atolgdansk') ?:'';
                $fields['atolgdansk_floor']['label'] = __($fields['atolgdansk_floor']['label'], 'atolgdansk') ?:'';
                $fields['atolgdansk_exposition']['label'] = __($fields['atolgdansk_exposition']['label'], 'atolgdansk') ?:'';
                $fields['atolgdansk_status']['label'] = __($fields['atolgdansk_status']['label'], 'atolgdansk') ?:'';
                $fields['atolgdansk_plan_view'] = wp_get_attachment_url(apply_filters( 'wpml_object_id', $fields['atolgdansk_plan_view'], 'attachment', TRUE  )) ?:'';
                $fields['virtual_walk'] = __($fields['virtual_walk'], 'atolgdansk') ?:'';
                       
            endif;

            $return[] = array( $fields );
        endwhile;
    endif;
    echo json_encode( $return );
    die;

// icl_object_id( $atolgdansk_proptype, 'osf_property', true, 'en' ),
}

/**
* Set up maisonco Child Theme's textdomain.
*
* Declare textdomain for this child theme.
* Translations can be added to the /languages/ directory.
*/
function atolgdansk_txtdomain_child_setup() {
load_child_theme_textdomain( 'atolgdansk', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'atolgdansk_txtdomain_child_setup' );


function apartment_search_include_short(){
ob_start();
//include the specified file
include('partials/apartments/apartments_search.php');
//assign the file output to $content variable and clean buffer
$content = ob_get_clean();
//return the $content
//return is important for the output to appear at the correct position
//in the content
return $content;
}
//register the Shortcode handler
add_shortcode('apartment_search_include', 'apartment_search_include_short');



add_filter( 'gform_notification_1', 'notify_every_2nd', 10, 3 );
function notify_every_2nd( $notification, $form, $entry ) {

// if ( $notification['name'] == 'Nowy lead - Jatczak (parzyste)' ) {
// //get the entry id and see if it is even
// if ( $entry['id'] % 2 == 0 ) {
// //return notification object
// return $notification;
// }
// } else if ( $notification['name'] == 'Nowy lead - Butkowski (nieparzyste)') { //
// if ( $entry['id'] % 2 != 0 ) {
// //return notification object
// return $notification;
// }
// } else {
// return $notification;
// }

if ( $notification['name'] == 'kzielinska_warynski, biuro_atolgdansk, biuro_jatczaknieruchomosci.pl - (parzyste)' ) {
//get the entry id and see if it is even
if ( $entry['id'] % 2 == 0 ) {
//return notification object
return $notification;
}
} else if ( $notification['name'] == 'kzielinska_warynski, biuro_atolgdansk, butowski_space-investment - (nieparzyste)')
{ //
if ( $entry['id'] % 2 != 0 ) {
//return notification object
return $notification;
}
} else {
return $notification;
}

}

function create_post_types()
{
register_post_type('garage',
array(
'labels' => array(
'name' => __('Garaże'),
'singular_name' => __('Garaże'),
'add_new' => __('Dodaj'),
'add_new_item' => __('Dodaj'),
'edit' => __('Edytuj'),
'edit_item' => __('Edytuj'),
'new_item' => __('Nowy'),
'view' => __('Nowy'),
'view_item' => __('Zobacz'),
'search_items' => __('Szukaj'),
'not_found' => __('Nie znaleziono'),
'not_found_in_trash' => __('Nie znaleziono')
),
'public' => true,
'supports' => array(
'title',
'custom-fields'
),
'can_export' => true,
'menu_icon' => 'dashicons-grid-view'
));

}

add_action('init', 'create_post_types');


function rkv_slideshow_redirect() {
global $wp_query;

// redirect from 'slideshow' CPT to home page
if ( is_archive('garage') || is_singular('garage') ) :
$url = get_bloginfo('url');

wp_redirect( esc_url_raw( $url ), 301 );
exit();
endif;
}

add_action ( 'template_redirect', 'rkv_slideshow_redirect', 1);