<?php
define( 'WP_CACHE', true );    // Added by WP Rocket.
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "atolgdansk" );

/** MySQL database username */
define( 'DB_USER', "root" );

/** MySQL database password */
define( 'DB_PASSWORD', "root" );

/** MySQL hostname */
define( 'DB_HOST', "localhost" );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?Tt*?yN=7fB2<|Lkbze],A$^VJ=/Z>qR>i}0 8^fl-0X-8[8F.Q[<Y}D(1et %M}');
define('SECURE_AUTH_KEY',  '}Xobv5]XTLLT&4U;2__8n4nCV={R?oH*9&?.Fj)>x/T[d~:=d<[1RNVai(n+$w@z');
define('LOGGED_IN_KEY',    '(qwM:uNtbVIFnUM}s~!^slbN<sXxgXz$k=$|}c8szCHDq-3>KQJvY$#GsK*txn>E');
define('NONCE_KEY',        '61GSUbm`}_z8T*Tlq_G!?{>2g 1AkF,*FtB>RDZ]~!q9=bf0)IX(4!08QXCG}w%l');
define('AUTH_SALT',        'o0z|n_3Dp{hke88pKR;]RU`W8nJl1RHh|mygk+UMLpyZ=Qp@n:VAdO}7H~}Z[=_A');
define('SECURE_AUTH_SALT', 'z%#Emo{MGdO*~w*1IZfNQ81M=@79(7fWNURy-5J?IF0zR#-1Q&&tVFL0CUqJt>F^');
define('LOGGED_IN_SALT',   '`K>,!sYH? zii*/`ADRrdE1an*+,D<w^8Xb^H(.d,JQ8M{y,7-?8f$%~;GH}.@}[');
define('NONCE_SALT',       '~uU*{AAivBud!!Wn>x~xxBULxy%rKLE>1]e^-k$}R49pm}N78@n7b#RG6oQ@<{x*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );

define( 'WP_DEBUG', false );
define( 'WP_DEBUG_LOG', false );
define( 'WP_DEBUG_DISPLAY', false );



@ini_set( 'upload_max_filesize' , '128M' );
@ini_set( 'upload_max_size' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'memory_limit', '256M' );


// Allow repairing the DB through /wp-admin/maint/repair.php
//define( 'WP_ALLOW_REPAIR', true );

/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */
// if ( ! defined( 'ABSPATH' ) ) {
// 	define( 'ABSPATH', '/home/os3/public_html/projekty.os3.pl/maisonco/' );
// }

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
